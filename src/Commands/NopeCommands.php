<?php

namespace Drupal\nope\Commands;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drush\Commands\DrushCommands;

class NopeCommands extends DrushCommands {

  /**
   * Delete orphaned entities.
   *
   * @command nope
   * @param $types Comma separated entity types to nix. Defaults to paragraph.
   */
  public function nope($types = 'paragraph') {
    $db = \Drupal::database();
    $tables = array_intersect_key($this->findTables(), array_flip(explode(',', $types)));
    foreach ($tables as $targetEntityTypeId => $referenceTableData) {
      $storage = \Drupal::entityTypeManager()->getStorage($targetEntityTypeId);
      $targetEntityType = $storage->getEntityType();
      $temp = $this->copyIdsToTemporaryTable($targetEntityType, $db);
      foreach ($referenceTableData as $tableData) {
        list($tableName, $column) = $tableData;
        $db->delete($temp)
          ->condition('id', $db->select($tableName, 't')->fields('t', [$column]), 'IN')
          ->execute();
      }
      $isRevisionable = $targetEntityType->isRevisionable();
      foreach ($db->select($temp, 't')->fields('t')->execute() as $row) {
        if ($isRevisionable) {
          try {
            $storage->deleteRevision($row->id);
          }
          catch (EntityStorageException $e) {
            // Default revisions can't be deleted. deleteOrphanEntities() will
            // delete them.
          }
        }
        elseif ($entity = $storage->load($row->id)) {
          $entity->delete();
        }
      }
      if ($isRevisionable) {
        $this->deleteOrphanEntities($storage, $db, $temp);
      }
    }
  }

  protected function findTables(): array {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $entityTypeManager = \Drupal::entityTypeManager();
    $tables = [];
    foreach ($entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      $entityStorage = $entityTypeManager->getStorage($entityTypeId);
      if ($entityStorage instanceof SqlEntityStorageInterface) {
        $mapping = $entityStorage->getTableMapping();
        if ($mapping instanceof DefaultTableMapping) {
          foreach ($entityFieldManager->getFieldStorageDefinitions($entityTypeId) as $fieldStorageDefinition) {
            if ($fieldStorageDefinition->getType() === 'entity_reference_revisions') {
              $targetTypeId = $fieldStorageDefinition->getSetting('target_type');
              $targetEntityType = $entityTypeManager->getDefinition($targetTypeId);
              $property = $targetEntityType->isRevisionable() ? 'target_revision_id' : 'target_id';
              $tables[$targetTypeId][] = [
                $this->getFieldTableName($entityType, $mapping, $fieldStorageDefinition),
                $mapping->getFieldColumnName($fieldStorageDefinition, $property),
              ];
            }
          }
        }
      }
    }
    return $tables;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   * @param \Drupal\Core\Entity\Sql\DefaultTableMapping $mapping
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $fieldStorageDefinition
   *
   * @return string
   */
  protected function getFieldTableName(EntityTypeInterface $entityType, DefaultTableMapping $mapping, FieldStorageDefinitionInterface $fieldStorageDefinition) {
    $requiresDedicatedTableStorage = $mapping->requiresDedicatedTableStorage($fieldStorageDefinition);
    if ($entityType->isRevisionable()) {
      if ($requiresDedicatedTableStorage) {
        return $mapping->getDedicatedRevisionTableName($fieldStorageDefinition);
      }
      else {
        return $entityType->getRevisionTable();
      }
    }
    else {
      if ($requiresDedicatedTableStorage) {
        return $mapping->getDedicatedDataTableName($fieldStorageDefinition);
      }
      else {
        return $entityType->getBaseTable();
      }
    }
  }


  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $targetEntityType
   * @param \Drupal\Core\Database\Connection $db
   *
   * @return string
   */
  protected function copyIdsToTemporaryTable(EntityTypeInterface $targetEntityType, Connection $db) {
    // The target surely is revisionable because otherwise a plain reference
    // field would be used but EntityReferenceRevisionsItem::schema() does
    // this check.
    if ($targetEntityType->isRevisionable()) {
      $targetTableName = $targetEntityType->getRevisionTable();
      $targetColumn = $targetEntityType->getKey('revision');
    }
    else {
      $targetTableName = $targetEntityType->getBaseTable();
      $targetColumn = $targetEntityType->getKey('id');
    }
    $select = $db->select($targetTableName, 't');
    $select->addField('t', $targetColumn, 'id');
    return $db->queryTemporary((string) $select);
  }

  /**
   * Delete orphaned enitties if they only have one revision left.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param \Drupal\Core\Database\Connection $db
   * @param string $temp
   *   Name of the temporary table containing revision ids slated for delete.
   */
  protected function deleteOrphanEntities(EntityStorageInterface $storage, Connection $db, string $temp) {
    $entityType = $storage->getEntityType();
    $entityIdColumn = $entityType->getKey('id');
    $revisionTable = $entityType->getRevisionTable();
    $select = $db->select($revisionTable, 't');
    $select->addField('t', $entityIdColumn, 'id');
    $select->condition($entityType->getKey('revision'), $db->select($temp, 't')->fields('t'), 'IN');
    // This table now contains the entity IDs of the revisions left.
    $temp2 = $db->queryTemporary((string) $select);
    // Find those with a single revision.
    /** @var \Drupal\Core\Database\Query\SelectInterface $select */
    $select = $db->select($revisionTable, 'r')
      ->condition($entityIdColumn, $db->select($temp2, 't')->fields('t'), 'IN');
    $select->addField('r', $entityIdColumn);
    $entityIds = $select
      ->groupBy($entityIdColumn)
      ->having('COUNT(*) = 1')
      ->execute()
      ->fetchCol();
    // Do not do a load multiple, there can be too many.
    foreach ($entityIds as $entityId) {
      if ($entity = $storage->load($entityId)) {
        $entity->delete();
      }
    }
  }

}
